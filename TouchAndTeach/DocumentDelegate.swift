import Foundation
import UIKit
import CoreGraphics

/**
 The DocumentDelegate protocol defines the methods called by the Document in response to important events
 
 - note: This delegate can ask for a value too (the frame size and the destination window)
 - author: Michaël Minelli
 - version: 1.0.0
 */

protocol DocumentDelegate {
    func document(document manager: Document, currentSlideDidChange newSlide: PaintView)
    func document(document manager: Document, changeName newName: String)
    
    func documentNeedUpdatedFrame(document manager: Document) -> CGRect
    func documentNeedWindow(document manager: Document) -> UIWindow?
}