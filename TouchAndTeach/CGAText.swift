import UIKit
import Foundation

/**
 The CGAText class describe a simple text (unicode not drawed)
 
 - note: For functions commentaries look at the CGAGeometry class commentaries
 - note: The state of this class is "in development" and is not finish because the wall doesn't support text
 - author: Michaël Minelli
 - version: 0.1.0
 */
class CGAText: NSObject, CGAGeometry, Serializable, Deserializable, NSCoding {
    private var paintView: PaintView = PaintView()
    
    var Uuid: String = ""
    
    private let mutexAlert:dispatch_semaphore_t = dispatch_semaphore_create(0)
    
    var start: CGAPoint?
    var stop: CGAPoint?
    
    /**
     Represent the CGRect which equal to the Rectangle coordinates and surface
     */
    private var rectangle: CGRect? {
        get {
            if let stop = self.stop, let start = self.start {
                return CGRect(x: start.preciseLocation.x, y: start.preciseLocation.y, width: stop.preciseLocation.x - start.preciseLocation.x, height: stop.preciseLocation.y - start.preciseLocation.y)
            }
            
            return nil
        }
    }
    private var text: String?
    
    private var objectCanceled = false
    
    //Label which show the text
    private var lbl: UITextView?
    
    private(set) var painter:CGAPainter = CGAPainter()
    
    var minX: CGFloat {
        get {
            return self.rectangle!.minX
        }
    }
    var minY: CGFloat {
        get {
            return self.rectangle!.minY
        }
    }
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    var jsonProperties:Array<String> = ["Start", "Stop", "MatrixTransform", "ObjectType", "Uuid", "Painter"]
    func jsonValue(forKey key: String) -> AnyObject? {
        switch key {
        case "Start":
            return self.start
        case "Stop":
            return self.stop
        case "MatrixTransform":
            return "1.0,0.0,0.0,1.0,0.0,0.0"
        case "Uuid":
            return self.Uuid
        case "Painter":
            return self.painter
        case "ObjectType":
            return "Text"
        default:
            return nil
        }
    }
    
    required init(fromJson json: AnyObject) {
        if let _ = json as? NSDictionary {
            //Not yet implemented on the wall
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        self.start = aDecoder.decodeObjectForKey("start") as! CGAPoint?
        self.stop = aDecoder.decodeObjectForKey("stop") as! CGAPoint?
        self.text = aDecoder.decodeObjectForKey("text") as! String?
        self.Uuid = aDecoder.decodeObjectForKey("Uuid") as! String
        self.objectCanceled = aDecoder.decodeBoolForKey("objectCanceled")
        self.painter = aDecoder.decodeObjectForKey("painter") as! CGAPainter
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.start, forKey: "start")
        aCoder.encodeObject(self.stop, forKey: "stop")
        aCoder.encodeObject(self.text, forKey: "text")
        aCoder.encodeObject(self.Uuid, forKey: "Uuid")
        aCoder.encodeObject(self.objectCanceled, forKey: "objectCanceled")
        aCoder.encodeObject(self.painter, forKey: "painter")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    init(withPainter painter: CGAPainter, onPaintView paintView: PaintView) {
        super.init()
        
        self.generateUuid()
        
        self.painter = painter
        self.paintView = paintView
    }
    
    func detected(touch touch: UITouch, withEvent event: UIEvent?) {
        if self.start != nil {
            self.stop = CGAPoint(fromTouch: touch)
        } else {
            self.start = CGAPoint(fromTouch: touch)
        }
    }
    
    func ended(touch touch: UITouch) {
        self.detected(touch: touch, withEvent: nil)
        
        //Show the alert view to enter the text to add
        let alert = UIAlertController(title: "Texte à insérer", message: "Veuillez entrer le texte à insérer.", preferredStyle: .Alert)
        
        let ok = UIAlertAction(title: "Insérer", style: .Default) { action -> Void in
            self.text = alert.textFields?[0].text
            self.paintView.setNeedsDisplayWithFrozenImageUpdated()
        }
        let cancel = UIAlertAction(title: "Annuler", style: .Cancel) { action -> Void in
            self.objectCanceled = true
            self.paintView.setNeedsDisplayWithFrozenImageUpdated()
        }
        
        alert.addAction(cancel)
        alert.addAction(ok)
        
        alert.addTextFieldWithConfigurationHandler { textField -> Void in
            textField.textColor = self.painter.color
        }
        
        self.paintView.controller.presentViewController(alert, animated: true, completion: nil)
    }
    
    func draw(inContext context: CGContext) {
        if !self.objectCanceled {
            if let text = self.text {
                //Show the label with the text
                
                lbl = UITextView(frame: self.rectangle!)
                lbl!.backgroundColor = UIColor.clearColor()
                lbl!.text = text
                lbl!.font = UIFont(name: lbl!.font!.fontName, size: 22)!
                lbl!.textColor = painter.color
                lbl!.userInteractionEnabled = true
                lbl!.multipleTouchEnabled = true
                self.paintView.controller.view.insertSubview(lbl!, belowSubview: self.paintView)
                
            } else if let rectangle = self.rectangle {
                //Show the rectangle of the label zone
                CGContextSetStrokeColorWithColor(context, UIColor.grayColor().CGColor)
                
                CGContextSetLineDash(context, 0.0, [5.0, 5.0], 2)
                CGContextSetLineWidth(context, 1.0)
                
                CGContextStrokeRect(context, rectangle)
            }
        }
    }
    
    /**
     Determine if a point is in the rectangle space
     
     - parameter point: Point to test
     
     - returns: Boolean that indicate if the point is in the rectangle
     */
    private func isInZone(forPosition point: CGPoint) -> Bool {
        if let rectangle = self.rectangle {
            return rectangle.contains(point)
        }
        
        return false
    }
    
    func isToErase(forTouch touch: UITouch) -> Bool {
        if let _ = self.rectangle where !objectCanceled {
            //Same logic as the rectangle (same logic as text in the Android app)
            let isPreviousTouchIn = isInZone(forPosition: touch.precisePreviousLocationInView(touch.view))
            let isActualTouchIn = isInZone(forPosition: touch.preciseLocationInView(touch.view))
            
            return isPreviousTouchIn == !isActualTouchIn
        }
        
        return false
    }
    
    func erase() {
        //Remove the label from the superview
        lbl!.removeFromSuperview()
    
        for view in self.paintView.controller.view.subviews {
            if let view = view as? UITextView {
                view.removeFromSuperview()
            }
        }
    }
    
    func pinchToZoom(fromGesture gesture: UIPinchGestureRecognizer) {
        //TODO: A FAIRE
    }
    
    func move(x x: CGFloat, y: CGFloat) {
        //TODO: A FAIRE
    }
}