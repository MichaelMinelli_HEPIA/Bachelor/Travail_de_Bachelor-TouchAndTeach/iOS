import UIKit

/**
 The RubberPickerManager class show a menu for select the rubber or the clean option
 
 - authors: Michaël Minelli
 - version: 1.0.0
 */
class RubberPickerManager: UIViewController {
    enum Mode {
        case Rubber, Clear
    }
    
    private static let WIDTH  = CGFloat(64)
    private static let HEIGHT = CGFloat(120)
    
    var delegate: RubberPickerManagerDelegate?
    
    /**
     Show the draw picker
     
     - parameter sourceView: View which call this function (generally a button)
     - parameter width:      Width of the source view
     - parameter controller: The current view controller
     */
    func show(fromView sourceView: UIView, ofWidth width: CGFloat, withViewController controller: UIViewController) {
        self.modalPresentationStyle = .Popover
        self.preferredContentSize = CGSizeMake(RubberPickerManager.WIDTH, RubberPickerManager.HEIGHT)
        
        if let popoverController = self.popoverPresentationController {
            popoverController.sourceView = sourceView
            popoverController.sourceRect = CGRect(x: 0, y: -1, width: width, height: 0)
            popoverController.permittedArrowDirections = .Any
            popoverController.backgroundColor = Colors.primaryColor
            popoverController.delegate = nil
        }
        
        controller.presentViewController(self, animated: true, completion: nil)
        
        self.view.backgroundColor = Colors.primaryColor
        
        //Change color of buttons
        for view in self.view.subviews {
            if let button = view as? UIButton {
                if let image = button.currentImage {
                    button.setImage(image.image(withColor: Colors.secondaryColor), forState: UIControlState.Normal)
                }
            }
        }
    }
    
    @IBAction func btRubberClick(sender: UIButton) {
        self.delegate?.rubberPicker(manager: self, buttonClick: .Rubber)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btClearClick(sender: UIButton) {
        self.delegate?.rubberPicker(manager: self, buttonClick: .Clear)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}