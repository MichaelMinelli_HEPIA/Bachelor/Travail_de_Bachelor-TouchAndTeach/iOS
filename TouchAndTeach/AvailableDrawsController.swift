import UIKit

/**
 The AvailableDrawsController class is the view controller of the list draws shared by the teacher
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class AvailableDrawsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var onSelectionAction: Optional<(_:String?) -> ()> = nil
    
    @IBOutlet weak var tableView: UITableView!
    
    var availableDrawName: [String] = []
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Declaration of the pull to refresh
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refreshDatas), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.startRefreshProximity()
    }
    
    /**
     Trigger the pull to refresh by fake scroll
     */
    func startRefreshProximity() {
        self.refreshControl.beginRefreshing()
        
        self.tableView.setContentOffset(CGPoint(x: 0, y: -self.refreshControl.frame.size.height), animated: true)
        
        self.refreshControl.sendActionsForControlEvents(UIControlEvents.ValueChanged)
    }
    
    /**
     Ask the wall application for the list of available draws
     */
    @objc private func refreshDatas() {
        //Test if the connection is available and get datas
        if let commCenter = Global_iOS.sharedInstance.commCenter where !commCenter.askAvailableDraws(withCompletionHandler: self.new) {
            if let onSelectionAction = self.onSelectionAction {
                onSelectionAction(nil)
            }
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    /**
     Called when the application receive the list of available draws
     
     - parameter datas: json received from the wall
     */
    func new(datas datas: String) {
        self.availableDrawName = []
        
        //Parse the json to retrieve the list
        do {
            let jsonObject = try NSJSONSerialization.JSONObjectWithData(datas.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions.MutableContainers)
            
            if let json = jsonObject as? NSDictionary {
                if let availableDraws = json["AvailableDraws"] as? NSArray {
                    for availableDraw in availableDraws {
                        if let name = availableDraw["drawName"] as? String {
                            self.availableDrawName.append(name)
                        }
                    }
                }
            }
        } catch {}
        
        self.tableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    @IBAction func btCancelClick(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.availableDrawName.count == 0 ? 1 : self.availableDrawName.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("drawName", forIndexPath: indexPath)
        
        //Test if there is at least one available draw
        if self.availableDrawName.count == 0 {
            cell.textLabel?.text = NSLocalizedString("AVDRAWS_NO_RESULT_FOUND", comment: "AVDRAWS_NO_RESULT_FOUND")
        } else {
            cell.textLabel?.text = self.availableDrawName[indexPath.row]
        }
        
        //Set the background of the row
        cell.backgroundColor = indexPath.row % 2 == 1 ? Colors.lineBackground2 : Colors.lineBackground1
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.availableDrawName.count > 0 {
            if let onSelectionAction = self.onSelectionAction {
                onSelectionAction(self.availableDrawName[indexPath.row])
            }
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}