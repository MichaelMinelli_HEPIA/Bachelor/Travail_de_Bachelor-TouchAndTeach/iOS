import Foundation
import UIKit

/**
 The CGAGeometryFactory class is the factory of CGAGeometry objects
 
 - seealso: [Design Pattern Factory](http://www.tutorialspoint.com/design_pattern/factory_pattern.htm)
 
 - note: This class contain only static functions
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CGAGeometryFactory {
    /**
     Obtain a geometric object by specify his type
     
     - parameter type:      Type of the wanted object
     - parameter painter:   Painter object
     - parameter paintView: The paintview which call this function
     - parameter touch:     The touch which have provoke the creation of the object
     
     - returns: The geometric object wanted
     */
    static func getGeometricObject(ofType type: CGAGeometryForm, withPainter painter: CGAPainter, forPaintView paintView: PaintView, withTouch touch: UITouch) -> CGAGeometry {
        switch type {
        case .Line:
            if touch.type == .Stylus {
                return CGALine(withPainter: painter)
            } else {
                return CGASmoothLine(withPainter: painter)
            }
        case .Rectangle:
            return CGARectangle(withPainter: painter)
        case .Ellipse:
            return CGAEllipse(withPainter: painter)
        case .StraightLine:
            return CGAStraightLine(withPainter: painter)
        case .Text:
            return CGAText(withPainter: painter, onPaintView: paintView)
        }
    }
    
    /**
     Obtain a geometric object by deserialize a json
     
     - parameter jsonDictionnary: Dictionnary from the parse of the json received from the wall
     
     - returns: The geometric object deserialized
     */
    static func getGeometricObject(fromJson jsonDictionnary: NSDictionary) -> CGAGeometry? {
        if let type = jsonDictionnary["ObjectType"] as? String {
            switch type {
            case "Stroke":
                if let strokeType = jsonDictionnary["StrokeType"] as? String where strokeType == "HighDensity" {
                    return CGALine(fromJson: jsonDictionnary)
                }
                
                return CGASmoothLine(fromJson: jsonDictionnary)
            case "Rectangle":
                return CGARectangle(fromJson: jsonDictionnary)
            case "Ellipse":
                return CGAEllipse(fromJson: jsonDictionnary)
            case "Line":
                return CGAStraightLine(fromJson: jsonDictionnary)
            case "Text":
                return CGAText(fromJson: jsonDictionnary)
            default:
                break
            }
        }
        
        return nil
    }
}