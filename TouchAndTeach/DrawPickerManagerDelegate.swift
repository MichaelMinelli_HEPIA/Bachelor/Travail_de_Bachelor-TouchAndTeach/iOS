import Foundation
import UIKit

/**
 The DrawPickerManagerDelegate protocol defines the methods called by the DrawPickerManager in response to important events like the draw mode changement
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
protocol DrawPickerManagerDelegate {
    func drawPicker(manager manager: DrawPickerManager, buttonClick newMode: DrawPickerManager.Mode)
}