import Foundation

/**
 The singleton Preferences class is use for load and store preferences of the app
 
 - note: This class is a singleton
 - author: Michaël Minelli
 - version: 1.0.0
 */
public class Preferences {
    
    private static let USERNAME = "Username"
    private static let USERNAME_DEFAULT = ""
    
    private init() { self.registerDefaults() }
    static let sharedInstance = Preferences()
    
    //Defaults value of preference
    private static let DEFAULTS:[String : AnyObject] = [:]
    
    private let standardDefault = NSUserDefaults.standardUserDefaults()
    
    /**
     Register default values of preferences
     */
    private func registerDefaults() {
        self.standardDefault.registerDefaults(Preferences.DEFAULTS)
        
        if self.standardDefault.objectForKey(Preferences.USERNAME) == nil {
            self.set(username: Preferences.USERNAME_DEFAULT)
        }
    }
    
    /**
     Load the username
     
     - returns: The UserName
     */
    func getUsername() -> String {
        let dataToLoad = self.standardDefault.objectForKey(Preferences.USERNAME) as! NSData
        return NSString(data: dataToLoad, encoding: NSUTF8StringEncoding) as! String
    }
    
    /**
     Store the username
     
     - parameter username: The UserName
     */
    func set(username username: String) {
        self.standardDefault.setObject(username.dataUsingEncoding(NSUTF8StringEncoding), forKey: Preferences.USERNAME)
        self.standardDefault.synchronize()
    }
}