import Foundation

import CocoaAsyncSocket

/**
 The UdpDiscoveryManager class is used for discover the server IP (TouchAndTeach for the wall) and his TCP socket port
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class UdpDiscoveryManager: GCDAsyncUdpSocketDelegate {
    static private let DISCOVERY_PORT: UInt16 = 1919
    static private let DISCOVERY_TIMEOUT = 5.0
    
    static private let BROADCAST_ADDRESS = "255.255.255.255"
    
    static private let MESSAGE_ASK = "AskPort"
    static private let MESSAGE_RESPONSE = "ServerPort"
    
    /**
     The UdpDiscoveryManager.Error enum contains all reasons that an error can append
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    enum Error {
        case BeginReceivingError, NotConnect, Unknown
    }

    var delegate: UdpDiscoveryManagerDelegate?
    
    private var udpSocket: GCDAsyncUdpSocket?
    private var timerTimeout: NSTimer = NSTimer()
    
    /**
     Close the socket and delete the reference to it
     */
    private func destroySocket() {
        self.udpSocket?.close()
        self.udpSocket = nil
    }
    
    /**
     Called when an error occured
     0. Stop the timer
     0. Send an event to the delegate
     0. Destroy the socket
     
     - note: If the socket is not destroy (closed isn't ennough) there is an error when we want to begin receiving again
     
     - parameter error: Error type to send to the delegate
     */
    private func errorAppend(withDescription error: Error) {
        self.timerTimeout.invalidate()
        self.delegate?.udpDiscovery(manager: self, errorOccurred: error)
        self.destroySocket()
    }
    
    /**
     Start a search of the server
     */
    func attemptToConnect() {
        //If the socket object doesn't exist, create it
        if self.udpSocket == nil {
            self.udpSocket = GCDAsyncUdpSocket(delegate: self, delegateQueue: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0))
            
            do {
                try self.udpSocket!.enableBroadcast(true)
                try self.udpSocket!.bindToPort(UdpDiscoveryManager.DISCOVERY_PORT)
                
                try self.udpSocket!.beginReceiving()
            } catch is GCDAsyncSocketError {
                self.errorAppend(withDescription: Error.BeginReceivingError)
                return
            } catch {
                self.errorAppend(withDescription: Error.Unknown)
                return
            }
        }
        
        //Send the ask request
        self.udpSocket!.sendData(UdpDiscoveryManager.MESSAGE_ASK.dataUsingEncoding(NSUTF8StringEncoding), toHost: UdpDiscoveryManager.BROADCAST_ADDRESS, port: UdpDiscoveryManager.DISCOVERY_PORT, withTimeout: UdpDiscoveryManager.DISCOVERY_TIMEOUT, tag: 0)
        
        //Start the timeout timer
        self.timerTimeout = NSTimer.scheduledTimerWithTimeInterval(UdpDiscoveryManager.DISCOVERY_TIMEOUT, target: self, selector: #selector(self.attemptTimedOut), userInfo: nil, repeats: false)
    }
    
    /**
     Function called if the limit time of a discover attempt is done
     */
    @objc private func attemptTimedOut() {
        self.destroySocket()
        self.delegate?.udpDiscovery(manager: self, connectionAttemptTimeout: UdpDiscoveryManager.DISCOVERY_TIMEOUT)
    }
    
    @objc func udpSocket(sock: GCDAsyncUdpSocket!, didReceiveData data: NSData!, fromAddress address: NSData!, withFilterContext filterContext: AnyObject!) {
        if let informations = NSString(data: data, encoding: NSASCIIStringEncoding) as? String {
            let informationsTable = informations.componentsSeparatedByString(":")
            
            if informationsTable[0] == UdpDiscoveryManager.MESSAGE_RESPONSE {
                self.timerTimeout.invalidate()
                
                //Extract server information from his reponse
                var host: NSString?
                var portSend: UInt16 = 0
                GCDAsyncUdpSocket.getHost(&host, port: &portSend, fromAddress: address)
                
                self.destroySocket()
                
                //Send to the delegate
                self.delegate?.udpDiscovery(manager: self, receiveConnectionInformations: ConnectionInformations(ip: host as! String, port: informationsTable[1]))
            }
        }
    }
    
    @objc func udpSocket(sock: GCDAsyncUdpSocket!, didNotConnect error: NSError!) {
        self.errorAppend(withDescription: Error.NotConnect)
    }
    
    @objc func udpSocket(sock: GCDAsyncUdpSocket!, didNotSendDataWithTag tag: Int, dueToError error: NSError!) {
        self.errorAppend(withDescription: Error.Unknown)
    }
}