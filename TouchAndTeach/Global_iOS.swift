import Foundation
import UIKit

import SCLAlertView

/**
 The singleton Global_iOS class contains object and functions which have to be accessible by all class in the iOS project
 
 - note: This class is a singleton
 - author: Michaël Minelli
 - version: 1.0.0
 */
class Global_iOS {
    
    var commCenter: CommCenter?
    var documentController: DocumentController = DocumentController()
    
    var alertAnimationStyle = SCLAnimationStyle.TopToBottom
    
    private init() { }
    static let sharedInstance = Global_iOS()
    
    /**
     Function called when the view controller have received a document. This function ask the user for the action to execute.
     
     - parameter document: Document received
     - parameter view:     View for display toast messages
     */
    func onReceive(document document: Document, onView view: UIView) {
        let alertView = SCLAlertView()
        
        alertView.addButton(NSLocalizedString("SAVE", comment: "SAVE")) { DocumentsManager.sharedInstance.askSave(document: document, onView: view) }
        alertView.addButton(NSLocalizedString("OPEN", comment: "OPEN")) { Global_iOS.sharedInstance.documentController.currentDocument = document }
        
        alertView.showCustom(NSLocalizedString("DOCUMENT_RECEIVED_TITLE", comment: "DOCUMENT_RECEIVED_TITLE"), subTitle: NSLocalizedString("DOCUMENT_RECEIVED_MSG", comment: "DOCUMENT_RECEIVED_MSG"), color: Colors.primaryColor, icon: UIImage(named: "iconDocument")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("IGNORE", comment: "IGNORE"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
    }
}