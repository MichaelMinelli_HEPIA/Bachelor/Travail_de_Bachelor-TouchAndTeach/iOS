import UIKit

/**
 The DrawPickerManager class show a menu for select the draw mode
 
 - authors: Michaël Minelli
 - version: 1.0.0
 */
class DrawPickerManager: UIViewController {
    enum Mode {
        case Ellipse, Rectangle, Line, Pen
    }
    
    private static let WIDTH  = CGFloat(64)
    private static let HEIGHT = CGFloat(208)
    
    var delegate: DrawPickerManagerDelegate?
    
    /**
     Show the draw picker
     
     - parameter sourceView: View which call this function (generally a button)
     - parameter width:      Width of the source view
     - parameter controller: The current view controller
     */
    func show(fromView sourceView: UIView, ofWidth width: CGFloat, withViewController controller: UIViewController) {
        self.modalPresentationStyle = .Popover
        self.preferredContentSize = CGSizeMake(DrawPickerManager.WIDTH, DrawPickerManager.HEIGHT)
        
        if let popoverController = self.popoverPresentationController {
            popoverController.sourceView = sourceView
            popoverController.sourceRect = CGRect(x: 0, y: -1, width: width, height: 0)
            popoverController.permittedArrowDirections = .Any
            popoverController.backgroundColor = Colors.primaryColor
            popoverController.delegate = nil
        }
        
        controller.presentViewController(self, animated: true, completion: nil)
        
        self.view.backgroundColor = Colors.primaryColor
        
        //Change color of buttons
        for view in self.view.subviews {
            if let button = view as? UIButton {
                if let image = button.currentImage {
                    button.setImage(image.image(withColor: Colors.secondaryColor), forState: UIControlState.Normal)
                }
            }
        }
    }
    
    @IBAction func btEllipseClick(sender: UIButton) {
        self.delegate?.drawPicker(manager: self, buttonClick: .Ellipse)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btRectangleClick(sender: UIButton) {
        self.delegate?.drawPicker(manager: self, buttonClick: .Rectangle)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btLineClick(sender: UIButton) {
        self.delegate?.drawPicker(manager: self, buttonClick: .Line)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btPenClick(sender: UIButton) {
        self.delegate?.drawPicker(manager: self, buttonClick: .Pen)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}