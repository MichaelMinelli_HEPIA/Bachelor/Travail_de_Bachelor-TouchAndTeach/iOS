import Foundation
import UIKit

import CocoaAsyncSocket
import Toast_Swift

/**
 The CommCenter class is used like a link between TouchAndTeach iOS and communication classes.
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
class CommCenter: UdpDiscoveryManagerDelegate, TcpCommunicationManagerDelegate {
    private static let FILTERED_MESSAGES = ["Err", "JpegMode", ""]
    /**
     The CommCenter.ConnectionState enum define the all different possible state of the connection to the server
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    enum ConnectionState: String {
        case Connected
        case Disconnected
    }
    
    /**
     The CommCenter.SendMessageType enum contains all message type that can be sent
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    private enum SendMessageType: String {
        case SendUserName
        case SendOnePage
        case SendOneJpeg
        case SendConnectionStatus
        case AskAvailableDraws
        case GetDraw
    }
    
    /**
     The CommCenter.ReceiveMessageType enum contains all message type that can be received
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    private enum ReceiveMessageType: String {
        case SendTextMessage
        case AvailableDraws
        case Draw
        case SendDocument
    }
    
    /**
     The ReceiveMode enum contains all receive mode
     
     - author: Michaël Minelli
     - version: 1.0.0
     */
    private enum ReceiveMode {
        case Normal
        case Document
    }
    
    var toastView: UIView?
    
    private let udpDiscoveryManager: UdpDiscoveryManager
    private let tcpCommunicationManager: TcpCommunicationManager
    
    private var receiveMode = ReceiveMode.Normal
    
    private var AvailableDrawsHandler: Optional<(_:String) -> ()> = nil
    
    private var DrawHandler: Optional<(_:String) -> ()> = nil
    
    /// Message (in NSData form) to send when the connection is done
    private var waitingMessage: NSData?
    
    /**
     Return the connection state
    */
    var state: ConnectionState {
        get {
            return tcpCommunicationManager.isConnected ? .Connected : .Disconnected
        }
    }
    
    var delegate: CommCenterDelegate?
    
    let mutexReceive = dispatch_semaphore_create(0) //Mutex for receive message per message (for the receive mode can be use correctly)
    
    init() {
        self.udpDiscoveryManager = UdpDiscoveryManager()
        self.tcpCommunicationManager = TcpCommunicationManager()
        
        self.udpDiscoveryManager.delegate = self
        self.tcpCommunicationManager.delegate = self
    }
    
    convenience init(withToastView view: UIView) {
        self.init()
        
        self.toastView = view
        
        dispatch_async(dispatch_get_main_queue(), {
            self.toastView?.makeToastActivity(.Center)
        })
    }
    
    /**
     Start the connection phase
     */
    func connect() {
        if !self.tcpCommunicationManager.isConnected {
            dispatch_async(dispatch_get_main_queue(), {
                self.toastView?.makeToastActivity(.Center)
            })
            
            self.udpDiscoveryManager.attemptToConnect()
        } else {
            self.showToast(message: NSLocalizedString("COMMUNICATION_ALREADY_CONNECTED", comment: "COMMUNICATION_ALREADY_CONNECTED"))
        }
    }
    
    /**
     Disconnect from the server
     */
    func disconnect() {
        if self.tcpCommunicationManager.isConnected {
            //TODO: Message de déconnection car ne détecte pas la fermeture d'une socket mais ce message et n'est pas implémenté du coté Android
            self.tcpCommunicationManager.send(message: self.buildMessage(ofType: .SendConnectionStatus, withMessage: ConnectionState.Disconnected.rawValue))
            
            self.tcpCommunicationManager.disconnect()
        }
    }
    
    /**
     Show, if possible, a toast message
     
     - parameter message: Message string to display
     */
    private func showToast(message message: String) {
        dispatch_async(dispatch_get_main_queue(), {
            self.toastView?.hideToastActivity()
            
            self.toastView?.makeToast(message)
        })
    }
    
    func udpDiscovery(manager manager: UdpDiscoveryManager, receiveConnectionInformations connectionInformations: ConnectionInformations) {
        self.tcpCommunicationManager.connect(toHost: connectionInformations)
    }
    
    func udpDiscovery(manager manager: UdpDiscoveryManager, connectionAttemptTimeout timeout: Double) {
        self.waitingMessage = nil
        
        self.showToast(message: NSLocalizedString("COMMUNICATION_NO_SERVER", comment: "COMMUNICATION_NO_SERVER"))
    }
    
    func udpDiscovery(manager manager: UdpDiscoveryManager, errorOccurred error: UdpDiscoveryManager.Error) {
        self.waitingMessage = nil
        
        self.showToast(message: NSLocalizedString("COMMUNICATION_ERROR_RETRY", comment: "COMMUNICATION_ERROR_RETRY"))
    }
    
    func tcpCommunication(manager manager: TcpCommunicationManager, isConnectedToHost host: ConnectionInformations) {
        self.showToast(message: NSLocalizedString("COMMUNICATION_CONNECTED", comment: "COMMUNICATION_CONNECTED"))
        
        self.delegate?.commCenter(commCenter: self, connectionStateChange: true)
        
        self.send(userName: Preferences.sharedInstance.getUsername())
        
        //If a message wait to be send, send it
        if let message = self.waitingMessage {
            self.tcpCommunicationManager.send(message: message)
            self.waitingMessage = nil
        }
    }
    
    func tcpCommunication(manager manager: TcpCommunicationManager, isDisconnectedFromHost host: ConnectionInformations) {
        self.showToast(message: NSLocalizedString("COMMUNICATION_DISCONNECTED", comment: "COMMUNICATION_DISCONNECTED"))
        
        self.delegate?.commCenter(commCenter: self, connectionStateChange: false)
    }
    
    func tcpCommunication(manager manager: TcpCommunicationManager, errorOccurred error: TcpCommunicationManager.Error) {
        self.waitingMessage = nil
        
        self.showToast(message: NSLocalizedString("COMMUNICATION_ERROR_RETRY", comment: "COMMUNICATION_ERROR_RETRY"))
    }
    
    func tcpCommunication(manager manager: TcpCommunicationManager, receiveMessage data: NSData) {
        dispatch_semaphore_wait(self.mutexReceive, 0)
        
        switch self.receiveMode {
        case .Document: //If we wait a document
            self.receiveMode = .Normal
            
            //Create a file from datas received
            let destinationPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] + "/TAT_FILE_RECEIVE_" + NSUUID().UUIDString
            let filePath = destinationPath + "/temp.tat"
            let destinationURL = NSURL(fileURLWithPath: destinationPath)
            
            //Create the directory
            do {
                try NSFileManager.defaultManager().createDirectoryAtURL(destinationURL, withIntermediateDirectories: true, attributes: nil)
                try data.writeToFile(filePath, options: NSDataWritingOptions.DataWritingAtomic)
            } catch {
                return
            }
            
            
            do {
                //Open a document from the file
                let document = try Document(fromFile: NSURL(fileURLWithPath: filePath))
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.delegate?.commCenter(commCenter: self, receiveDocument: document)
                })
            } catch {
                return
            }
        case .Normal:
            let receiveMessage = NSString(data: data, encoding: NSUTF8StringEncoding)!.stringByReplacingOccurrencesOfString("\r\n", withString: "")
            
            //Received message
            let messageTable = receiveMessage.componentsSeparatedByString(":")
            let command = ReceiveMessageType(rawValue: messageTable[0])
            let message = receiveMessage.substringFromIndex(receiveMessage.startIndex.advancedBy(messageTable[0].characters.count + 1))
            
            //Test the type of the message
            if let command = command {
                switch command {
                case .SendTextMessage:
                    if !CommCenter.FILTERED_MESSAGES.contains(message) {
                        self.showToast(message: NSLocalizedString(message, comment: message))
                    }
                case .AvailableDraws:
                    if let action = AvailableDrawsHandler {
                        action(message)
                    }
                case .Draw:
                    if let action = DrawHandler {
                        action(message)
                    }
                case .SendDocument:
                    self.receiveMode = .Document
                    self.tcpCommunicationManager.startReadData(toLength: UInt(message)!)
                }
            }
        }
        
        dispatch_semaphore_signal(self.mutexReceive)
    }
    
    
    private static let MESSAGE_SEPARATOR = ":"
    
    /**
     Build a message by create the commande string and convert it to NSData
     
     - parameter type:    Type of the send message (SendMessageType)
     - parameter message: Message string to send
     
     - returns: Built message (NSData)
     */
    private func buildMessage(ofType type: SendMessageType, withMessage message: String) -> NSData {
        let data = NSMutableData(data: (type.rawValue + CommCenter.MESSAGE_SEPARATOR + message).dataUsingEncoding(NSUTF8StringEncoding)!)
        
        //Append this for signify the end of the message to the server socket
        data.appendData(GCDAsyncSocket.CRLFData())
        
        return data
    }
    
    /**
     Send message if possible otherwise set the message in waiting mode and start a connection attempt
     
     - parameter message: Message to send (NSData) already built
     */
    private func send(message message: NSData) {
        if self.tcpCommunicationManager.isConnected {
            self.tcpCommunicationManager.send(message: message)
        } else {
            self.waitingMessage = message
            self.connect()
        }
    }
    
    /**
     Send the username to the server
     */
    func send(userName userName: String) {
        self.send(message: buildMessage(ofType: .SendUserName, withMessage: userName))
    }
    
    /**
     Send a slide (PaintView) to the server
     
     - parameter slide: The PaintView to send
     */
    func send(oneSlide slide: PaintView) {
        self.send(message: buildMessage(ofType: .SendOnePage, withMessage: Serialize.toJSON(fromSerializable: slide)))
    }
    
    /**
     Send a picture to the server
     0. Send a message that inform that a picture have to be received with his size in bytes
     0. Send the picture data
     0. Send a end message
     
     - parameter image: Picture to send (UIImage)
     */
    func send(image image: UIImage) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            let data = NSMutableData(data: UIImageJPEGRepresentation(image, 1.0)!)
            data.appendData(GCDAsyncSocket.CRLFData())
            
            self.send(message: self.buildMessage(ofType: .SendOneJpeg, withMessage: String(data.length)))
            self.send(message: data)
        })
    }
    
    /**
     Ask the server for obtain the list of available draws
     
     - parameter action: Handler for data reception
     
     - returns: false if no connection to the server is available
     */
    func askAvailableDraws(withCompletionHandler action: (_:String) -> ()) -> Bool {
        if self.tcpCommunicationManager.isConnected {
            self.AvailableDrawsHandler = action
            
            self.tcpCommunicationManager.send(message: self.buildMessage(ofType: .AskAvailableDraws, withMessage: ""))
            
            return true
        }
        
        return false
    }
    
    /**
     Ask the server for obtain a specifiq shared draw
     
     - parameter name: Name of the wanted slide
     - parameter action: Handler for data reception
     
     - returns: false if no connection to the server is available
     */
    func askDraw(withName name: String, withCompletionHandler action: (_:String) -> ()) -> Bool {
        if self.tcpCommunicationManager.isConnected {
            self.DrawHandler = action
            
            self.tcpCommunicationManager.send(message: self.buildMessage(ofType: .GetDraw, withMessage: name))
            
            return true
        }
        
        return false
    }
}