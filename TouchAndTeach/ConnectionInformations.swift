/**
 This struct contains informations of a connection (to the server)
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
struct ConnectionInformations {
    var ip: String
    var port: UInt16
    
    init(ip: String, port: UInt16) {
        self.ip = ip
        self.port = port
    }
    
    init(ip: String, port: String) {
        self.init(ip: ip, port: UInt16(port)!)
    }
}