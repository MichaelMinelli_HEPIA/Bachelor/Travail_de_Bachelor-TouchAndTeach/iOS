import UIKit

import SCLAlertView

/**
 The MainController class is the principal view controller of the project. It's the view which contains all PaintViews

 - author: Michaël Minelli
 - version: 1.0.0
 */
class MainController: UIViewController, CommCenterDelegate, ColorPickerManagerDelegate, DrawPickerManagerDelegate, RubberPickerManagerDelegate, DocumentDelegate, PaintViewDelegate {
    
    @IBOutlet weak var viewForPaint: UIView!

    @IBOutlet weak var viewToolBox: UIView!
    @IBOutlet weak var stackToolBox: UIStackView!
    
    @IBOutlet weak var btColorPicker: SelectColorButton!
    @IBOutlet weak var btDrawPicker: UIButton!
    @IBOutlet weak var btRubberPicker: UIButton!
    @IBOutlet weak var btResize: UIButton!
    
    //Picker managers
    var colorPickerManager = ColorPickerManager()
    var drawPickerManager = DrawPickerManager()
    var rubberPickerManager = RubberPickerManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the CommCenter connection delegate
        Global_iOS.sharedInstance.commCenter?.delegate = self
        
        self.viewToolBox.backgroundColor = Colors.primaryColor
        
        //Change color of buttons
        for view in self.stackToolBox.subviews {
            if let button = view as? UIButton {
                if let image = button.currentImage {
                    button.setImage(image.image(withColor: Colors.secondaryColor), forState: UIControlState.Normal)
                }
            }
        }
        
        //Set picker managers
        self.colorPickerManager = storyboard?.instantiateViewControllerWithIdentifier("colorPicker") as! ColorPickerManager
        self.colorPickerManager.delegate = self
        self.drawPickerManager = storyboard?.instantiateViewControllerWithIdentifier("drawPicker") as! DrawPickerManager
        self.drawPickerManager.delegate = self
        self.rubberPickerManager = storyboard?.instantiateViewControllerWithIdentifier("rubberPicker") as! RubberPickerManager
        self.rubberPickerManager.delegate = self
        
        Global_iOS.sharedInstance.documentController.documentDelegate = self
        Global_iOS.sharedInstance.documentController.paintViewDelegate = self
        
        self.updateTitle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.displayCurrentPaintView()
        
        //Interface initialisation
        self.initGlobalAppearance()
        self.setNavigationBarItem(withName: NSLocalizedString("MENU_ITEM_NORMAL_MODE", comment: "MENU_ITEM_NORMAL_MODE"), isConnected: Global_iOS.sharedInstance.commCenter?.state == .Connected)
    }
    
    func commCenter(commCenter commCenter: CommCenter, connectionStateChange connected: Bool) {
        self.setNavigationBarItem(withName: NSLocalizedString("MENU_ITEM_NORMAL_MODE", comment: "MENU_ITEM_NORMAL_MODE"), isConnected: connected)
    }
    
    func commCenter(commCenter commCenter: CommCenter, receiveDocument document: Document) {
        Global_iOS.sharedInstance.onReceive(document: document, onView: self.view)
    }
    
    //////////////////////////////////////////////////////////// PaintView //////////////////////////////////////////////////////////////
    /**
     Update the title of the view controller with the name of the document ans slide
     */
    func updateTitle() {
        var leftValue = Global_iOS.sharedInstance.documentController.currentDocument.name
        var separator = " - "
        var rightValue = Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.name
        
        if let rightValue = rightValue where rightValue != "" {
            if leftValue == "" {
                leftValue = NSLocalizedString("DOCUMENT_NO_NAME", comment: "DOCUMENT_NO_NAME")
            }
        } else {
            separator = ""
            rightValue = ""
        }
        
        self.title = leftValue + separator + rightValue!
    }
    
    func document(document manager: Document, currentSlideDidChange newSlide: PaintView) {
        self.updateTitle()
        self.replaceCurrentPaintView(with: newSlide)
    }
    
    func document(document manager: Document, changeName newName: String) {
        self.updateTitle()
    }
    
    func documentNeedUpdatedFrame(document manager: Document) -> CGRect {
        return CGRect(x: 0, y: 0, width: self.viewForPaint.frame.size.width, height: self.viewForPaint.frame.size.height)
    }
    
    func documentNeedWindow(document manager: Document) -> UIWindow? {
        return self.view.window
    }
    
    func paintView(manager: PaintView, changeName newName: String?) {
        self.updateTitle()
    }
    
    /**
     Function called for raplace the current showed paintview by an another (most of the time for change document or change a slide)
     
     - parameter newPaintView: New paintview to show
     */
    private func replaceCurrentPaintView(with newPaintView: PaintView) {
        //Remove old paintview from the interface
        for view in self.viewForPaint.subviews {
            if let view = view as? PaintView {
                view.removeFromSuperview()
            }
        }
        
        self.btColorPicker.selectedColor = newPaintView.nextGeometricObjectColor
        self.btColorPicker.selectedWidth = CGFloat(newPaintView.nextGeometricObjectThickness)
        
        newPaintView.frame = CGRect(x: 0, y: 0, width: self.viewForPaint.frame.size.width, height: self.viewForPaint.frame.size.height)

        self.viewForPaint.addSubview(newPaintView)
        
        self.resetToolBox()
    }
    
    /**
     Display the current selected slide
     */
    func displayCurrentPaintView() {
        self.replaceCurrentPaintView(with: Global_iOS.sharedInstance.documentController.currentDocument.currentSlide)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {

    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        Global_iOS.sharedInstance.documentController.currentDocument.set(frame: CGRect(x: 0, y: 0, width: self.viewForPaint.frame.size.width, height: self.viewForPaint.frame.size.height), andWindow: self.view.window)
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    //////////////////////////////////////////////////////////// ToolBox //////////////////////////////////////////////////////////////
    /**
     Reset the toolbox state by reset all background color and update the background and the icon of wich is selected
     */
    private func resetToolBox() {
        //Remove all background color
        for view in self.stackToolBox.subviews {
            if let button = view as? UIButton {
                if let _ = button.currentImage {
                    button.backgroundColor = UIColor.clearColor()
                }
            }
        }
        
        //Show the current button and image to it
        var btToAccentuate: UIButton
        
        switch Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectTouchMode {
        case .Draw:
            var image: UIImage
            
            switch Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextDrawType {
            case .Ellipse:
                image = UIImage(named: "paintEllipse")!
            case .Rectangle:
                image = UIImage(named: "paintRectangle")!
            case .StraightLine:
                image = UIImage(named: "paintLine")!
            case .Line:
                image = UIImage(named: "paintPen")!
            default:
                image = UIImage(named: "paintPen")!
            }
            
            btToAccentuate = self.btDrawPicker
            self.btDrawPicker.setImage(image.image(withColor: Colors.secondaryColor), forState: UIControlState.Normal)
        case .Resize:
            btToAccentuate = self.btResize
        case .Rubber:
            btToAccentuate = self.btRubberPicker
        }
        
        btToAccentuate.backgroundColor = Colors.accentuatedPrimaryColor
    }
    
    @IBAction func btColorPickerClick(sender: UIButton) {
        self.colorPickerManager.show(fromView: sender, ofWidth: sender.bounds.width, withViewController: self, withColor: Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectColor, withThickness: Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectThickness)
    }
    
    func colorPicker(manager manager: ColorPickerManager, selectNewColor color: UIColor) {
        Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectColor = color
        self.btColorPicker.selectedColor = color
    }
    
    func colorPicker(manager manager: ColorPickerManager, selectNewThickness thickness: Float) {
        Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectThickness = thickness
        self.btColorPicker.selectedWidth = CGFloat(thickness)
    }
    
    @IBAction func btDrawPickerClick(sender: UIButton) {
        self.drawPickerManager.show(fromView: sender, ofWidth: sender.bounds.width, withViewController: self)
    }
    
    func drawPicker(manager manager: DrawPickerManager, buttonClick newMode: DrawPickerManager.Mode) {
        Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectTouchMode = .Draw
        
        switch newMode {
        case .Ellipse:
            Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextDrawType = .Ellipse
        case .Rectangle:
            Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextDrawType = .Rectangle
        case .Line:
            Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextDrawType = .StraightLine
        case .Pen:
            Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextDrawType = .Line
        }
        
        self.resetToolBox()
    }
    
    @IBAction func btRubberPickerClick(sender: UIButton) {
        self.rubberPickerManager.show(fromView: sender, ofWidth: sender.bounds.width, withViewController: self)
    }
    
    func rubberPicker(manager manager: RubberPickerManager, buttonClick newMode: RubberPickerManager.Mode) {
        if newMode == .Rubber {
            Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectTouchMode = .Rubber
            self.resetToolBox()
        } else {
            //Show a prompt message to ask if the user is compeltly sure to remove all element from the slide
            let alertView = SCLAlertView()
            
            alertView.addButton(NSLocalizedString("YES", comment: "YES")) { Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.clear() }
            
            alertView.showCustom(NSLocalizedString("PAINTVIEW_CLEAR_TITLE", comment: "PAINTVIEW_CLEAR_TITLE"), subTitle: NSLocalizedString("PAINTVIEW_CLEAR_MSG", comment: "PAINTVIEW_CLEAR_MSG"), color: Colors.primaryColor, icon: UIImage(named: "paintRubber")!.image(withColor: Colors.secondaryColor), closeButtonTitle: NSLocalizedString("CANCEL", comment: "CANCEL"), duration: 0.0, colorStyle: 0, colorTextButton: UInt(Colors.secondaryColor.argb!), circleIconImage: nil, animationStyle: Global_iOS.sharedInstance.alertAnimationStyle)
        }
    }
    
    @IBAction func btUndoClick(sender: UIButton) {
        Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.undo()
    }
    
    @IBAction func btRedoClick(sender: UIButton) {
        Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.redo()
    }
    
    @IBAction func btResizeClick(sender: UIButton) {
        Global_iOS.sharedInstance.documentController.currentDocument.currentSlide.nextGeometricObjectTouchMode = .Resize
        self.resetToolBox()
    }
    
    @IBAction func btPreviousSlideClick(sender: UIButton) {
        Global_iOS.sharedInstance.documentController.currentDocument.changeCurrentSlide(toNewSlideIndex: Global_iOS.sharedInstance.documentController.currentDocument.currentSlideIndex - 1)
    }
    
    @IBAction func btNextSlideClick(sender: UIButton) {
        Global_iOS.sharedInstance.documentController.currentDocument.changeCurrentSlide(toNewSlideIndex: Global_iOS.sharedInstance.documentController.currentDocument.currentSlideIndex + 1)
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}