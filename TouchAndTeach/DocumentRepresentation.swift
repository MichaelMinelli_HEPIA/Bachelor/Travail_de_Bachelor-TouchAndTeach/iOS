import Foundation
import UIKit

/**
 The DocumentRepresentation class describe principal property of a document
 
 - note: This class is used by the list of stored document of the singleton class named DocumentsManager
 - author: Michaël Minelli
 - version: 1.0.0
 */
class DocumentRepresentation: NSObject, NSCoding {
    var name: String
    var image: UIImage?
    var lastSave: NSDate
    
    ////////////////////////////////////////////////////////////////// Coding /////////////////////////////////////////////////////////////////////
    required init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObjectForKey("name") as! String
        self.image = aDecoder.decodeObjectForKey("image") as? UIImage
        self.lastSave = aDecoder.decodeObjectForKey("lastSave") as! NSDate
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.name, forKey: "name")
        aCoder.encodeObject(self.image, forKey: "image")
        aCoder.encodeObject(self.lastSave, forKey: "lastSave")
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    init(withDocument document: Document) {
        self.name = document.name
        if let image = document.getSlide(at: 0)!.image {
            self.image = image
        }
        self.lastSave = NSDate()
        
        super.init()
    }
    
    init(withName name: String) {
        self.name = name
        self.lastSave = NSDate()
    }
    
    /**
     Update the save time to the current
     */
    func updateSaveToNow() {
        self.lastSave = NSDate()
    }
}

func ==(lhs: DocumentRepresentation, rhs: DocumentRepresentation) -> Bool {
    return lhs.name == rhs.name
}