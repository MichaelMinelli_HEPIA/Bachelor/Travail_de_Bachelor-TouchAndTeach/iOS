import UIKit

/**
 Description
 ---
 UIButton interface for show color and thickness of a line
 
 Changement from original code
 ---
 * Change the round design to a line design
 
 - authors: Michaël Minelli, Arnaud Thiercelin
 - seealso: [Original code](https://github.com/athiercelin/ATSketchKit/blob/master/ATSketchKitDemo/ATBrushButton.swift) -- ATBrushButton class From the ATSketchKit project demo app
 - version: 2.0.0
 */
@IBDesignable
class SelectColorButton: UIButton {

	private var _selectedWidth = CGFloat(1.0)
	@IBInspectable var selectedWidth: CGFloat {
		get {
			return self._selectedWidth
		}
		set {
			self._selectedWidth = newValue
			self.setNeedsDisplay()
		}
	}
    
    private var _selectedColor = UIColor.blackColor()
    @IBInspectable var selectedColor: UIColor {
        get {
            return _selectedColor
        }
        set {
            _selectedColor = newValue
            self.setNeedsDisplay()
        }
    }
	
	override func drawRect(rect: CGRect) {
        let lineRect = CGRect(x: rect.width / 4, y: (rect.maxY - rect.minY) / 2 - self.selectedWidth / 2, width: rect.width / 2, height: self.selectedWidth)
        
        let path = UIBezierPath(roundedRect: lineRect, cornerRadius: 0.8 * self.selectedWidth)
        self.selectedColor.setFill()
        path.fill()
	}
}
