import Foundation

/**
 The Deserializable protocol describe function that an object have to implement for being json deserializable
 
 - author: Michaël Minelli
 - version: 1.0.0
 */
protocol Deserializable {
    init(fromJson json: AnyObject)
}